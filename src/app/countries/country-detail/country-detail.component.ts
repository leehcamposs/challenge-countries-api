import { Component, OnInit } from '@angular/core';

import { CountryDetailService } from './country-detail.service';
import { CountryDetail } from '../models/country-detail';

@Component({
  selector: 'app-country-detail',
  templateUrl: './country-detail.component.html',
  styleUrls: ['./country-detail.component.scss']
})
export class CountryDetailComponent implements OnInit {

  public country = [];

  constructor(private countryDetailService: CountryDetailService) { }

  ngOnInit(): void {
    this.getCountryDetailAPI();
  }

  async getCountryDetailAPI() {
     this.countryDetailService.getCountryDetail('belgium')
      .subscribe((response) => {
        this.setCountryDetail(response);
      });
  }

  setCountryDetail(items: CountryDetail[]) {

    items.map((item) => {
      this.country.push({
        flag: item.flag,
        name: item.name,
        nativeName: item.nativeName,
        population: item.population,
        region: item.region,
        subregion: item.subregion,
        capital: item.capital,
        topLevelDomain: item.topLevelDomain[0],
        currencies: item.currencies[0].name,
        languages: item.languages,
        borders: item.borders,
      });
    });

    console.log('country 2', this.country);
  }

}
