export interface CountryDetail {
  flag: string;
  name: string;
  nativeName: string;
  population: number;
  region: string;
  subregion: string;
  capital: string;
  topLevelDomain: string;
  currencies: Array<Currencies>;
  languages: Array<string>;
  borders: string;
}

export interface Currencies {
  name: string;
}
