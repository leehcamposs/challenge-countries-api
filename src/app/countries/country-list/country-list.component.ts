import { Component, OnInit } from '@angular/core';

import { CountryList } from './../models/country-list';
import { CountryListService } from './country-list.service';

@Component({
  selector: 'app-country-list',
  templateUrl: './country-list.component.html',
  styleUrls: ['./country-list.component.scss']
})
export class CountryListComponent implements OnInit {

  public countries = [];

  constructor(private countryListService: CountryListService) { }

  ngOnInit(): void {
    this.getCountriesAPI();
  }

  async getCountriesAPI() {
    await this.countryListService.getCountries()
      .subscribe((country: CountryList[]) => {
        this.setCountries(country);
      });
  }

  setCountries(country: CountryList[]): void {

    country.map((item) => {
      this.countries.push({
        name: item.name,
        capital: item.capital,
        population: item.population,
        region: item.region,
        flag: item.flag
      });
    });

  }

}
