import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';

import { CountryListComponent } from './country-list/country-list.component';
import { CountryDetailComponent } from './country-detail/country-detail.component';
import { CardModule } from '../shared/components/card/card.module';

@NgModule({
  declarations: [CountryListComponent, CountryDetailComponent],
  imports: [
    CommonModule,
    CardModule,
    MatCardModule,
    MatGridListModule
  ],
  exports: [CountryListComponent, CountryDetailComponent]
})
export class CountriesModule { }
